#
# Be sure to run `pod lib lint Holus.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Holus'
  s.version          = '0.0.1'
  s.summary          = 'Holus is used for ID Verification'
  s.homepage         = 'https://gitlab.com/frslabs-public/ios/holus'
  s.license          = 'MIT'
  s.author           = { 'ashish' => 'ashish@frslabs.com' }
  s.source           = { :http => 'https://holus-ios.repo.frslabs.space/holus-ios/0.0.1/Holus.framework.zip'}
  s.platform         = :ios
  s.ios.deployment_target = '11.0'
  s.ios.vendored_frameworks = 'Holus.framework'
  s.swift_version = '5.0'
end
